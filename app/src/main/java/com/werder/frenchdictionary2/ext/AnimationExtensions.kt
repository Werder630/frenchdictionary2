package com.werder.frenchdictionary2.ext

import android.view.animation.Animation

fun Animation.onEnd(block: () -> Unit): Animation {
    setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationEnd(animation: Animation?) {
            block()
        }

        override fun onAnimationRepeat(animation: Animation?) {}
        override fun onAnimationStart(animation: Animation?) {}
    })
    return this
}