package com.werder.frenchdictionary2.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.werder.frenchdictionary2.db.WordEntity

class WordViewModel : ViewModel() {
    val allWords: LiveData<List<WordEntity>> = WordRepository.words
    var currentWord: MutableLiveData<WordEntity> = MutableLiveData()

    var currentPosition: Int = 1
        set(value) {
            field = value
            currentWord.value = WordRepository.wordByPosition(value)
        }

    init {
        currentPosition = 1
    }

    fun next() {
        if (currentPosition == allWords.value!!.size) currentPosition = 1 else currentPosition++
    }

    fun previous() {
        if (currentPosition == 1) currentPosition = allWords.value!!.size else currentPosition--
    }

    fun updateRememberState() {
        val current = currentWord.value!!
        current.remembered = !current.remembered
        WordRepository.update(current)
    }

    fun allWordCount() = WordRepository.allWordsCount

    fun rememberedWordCount() = WordRepository.rememberedWordCount
}