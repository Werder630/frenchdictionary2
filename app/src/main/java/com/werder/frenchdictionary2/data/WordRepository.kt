package com.werder.frenchdictionary2.data

import androidx.lifecycle.LiveData
import com.werder.frenchdictionary2.db.WordDatabase
import com.werder.frenchdictionary2.db.WordEntity

object WordRepository {
    val words: LiveData<List<WordEntity>> = WordDatabase.instance.wordDao().getWords()

    val allWordsCount: Int
        get() {
            return WordDatabase.instance.wordDao().getCount()
        }

    val rememberedWordCount: Int
        get() {
            return WordDatabase.instance.wordDao().getRememberedCount()
        }

    fun wordByPosition(position: Int): WordEntity = WordDatabase.instance.wordDao().getWordByPosition(position)

    fun update(wordEntity: WordEntity) {
        WordDatabase.instance.wordDao().update(wordEntity)
    }
}