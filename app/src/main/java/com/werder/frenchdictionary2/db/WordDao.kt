package com.werder.frenchdictionary2.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update

@Dao
interface WordDao {
    @Query("SELECT * FROM words")
    fun getWords(): LiveData<List<WordEntity>>

    @Query("SELECT * FROM words WHERE id = :position LIMIT 1")
    fun getWordByPosition(position: Int): WordEntity

    @Query("SELECT count(*) FROM words")
    fun getCount(): Int

    @Query("SELECT count(*) FROM words WHERE remembered = 1")
    fun getRememberedCount(): Int

    @Update
    fun update(entity: WordEntity)
}