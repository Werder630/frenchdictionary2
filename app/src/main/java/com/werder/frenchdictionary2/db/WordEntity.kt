package com.werder.frenchdictionary2.db

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "words")
@Parcelize
data class WordEntity(
        @ColumnInfo(name = "id")
        @PrimaryKey var id: Int = 0,
        @ColumnInfo(name = "word")
        val word: String = "",
        @ColumnInfo(name = "translate")
        val translate: String = "",
        @ColumnInfo(name = "example")
        val example: String = "",
        @ColumnInfo(name = "translate_example")
        val translateExample: String = "",
        @ColumnInfo(name = "source")
        val source: String = "",
        @ColumnInfo(name = "author")
        val author: String = "",
        @ColumnInfo(name = "remembered")
        var remembered: Boolean = false
) : Parcelable