package com.werder.frenchdictionary2.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.werder.frenchdictionary2.App
import java.io.FileOutputStream
import java.io.IOException

private const val DATABASE_NAME = "introductory.db"

@Database(entities = [WordEntity::class], version = 1)
abstract class WordDatabase : RoomDatabase() {
    abstract fun wordDao(): WordDao

    companion object {
        val instance: WordDatabase by lazy {
            val context = App.applicationContext()
            copyAttachedDatabase(context)
            Room.databaseBuilder(context, WordDatabase::class.java, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build()
        }

        private fun copyAttachedDatabase(context: Context) {
            val dbPath = context.getDatabasePath(DATABASE_NAME)

            if (dbPath.exists()) {
                return
            }

            dbPath.parentFile.mkdirs()

            try {
                val inputStream = context.assets.open(DATABASE_NAME)
                val output = FileOutputStream(dbPath)

                val buffer = ByteArray(8192)

                while (true) {
                    val length = inputStream.read(buffer, 0, 8192)
                    if (length <= 0) {
                        break
                    }
                    output.write(buffer, 0, length)
                }

                output.flush()
                output.close()
                inputStream.close()
            } catch (e: IOException) {
                throw RuntimeException("Read database failed: " + e.message)
            }
        }
    }
}