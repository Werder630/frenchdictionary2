package com.werder.frenchdictionary2.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.werder.frenchdictionary2.R
import com.werder.frenchdictionary2.data.WordRepository
import com.werder.frenchdictionary2.data.WordViewModel
import com.werder.frenchdictionary2.db.WordEntity
import kotlinx.android.synthetic.main.fragment_list.recycler
import kotlinx.android.synthetic.main.fragment_list.toolbar

const val POSITION_KEY = "PositionKey"

class ListFragment : Fragment(), Filterable {

    private val wordAdapter = WordAdapter()
    private lateinit var viewModel: WordViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? AppCompatActivity)!!.setSupportActionBar(toolbar)
        toolbar.title = "Словарь"
        setHasOptionsMenu(true)

        recycler.adapter = wordAdapter
        recycler.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(WordViewModel::class.java)

        viewModel.allWords.observe(this, Observer<List<WordEntity>> { wordAdapter.submitList(it) })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_list, menu)
        (menu.findItem(R.id.search).actionView as SearchView).apply {
            queryHint = "Поиск"
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?) = false

                override fun onQueryTextChange(newText: String?): Boolean {
                    filter.filter(newText)
                    return true
                }
            })
        }
    }

    override fun getFilter() = object : Filter() {

        override fun performFiltering(constraint: CharSequence): FilterResults {
            val list = (WordRepository.words.value)?.asSequence()
            val temp = mutableListOf<WordEntity>()
            list?.forEach {
                if (it.word.contains(constraint, true)) {
                    temp += it
                }
            }

            return FilterResults().apply {
                count = temp.size
                values = temp
            }
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            @Suppress("UNCHECKED_CAST")
            wordAdapter.submitList(results.values as ArrayList<WordEntity>)
        }
    }

    private inner class WordAdapter : ListAdapter<WordEntity, RecyclerView.ViewHolder>(DiffUtils) {

        private var words: List<WordEntity>? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val root = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false)
            return Holder(root)
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            words?.get(position)?.let { entity ->
                (holder as Holder).apply {
                    title.apply {
                        text = entity.word
                        background = ColorDrawable(if (entity.remembered) Color.parseColor("#33b8e986") else Color.TRANSPARENT)
                    }
                    subtitle.text = entity.translate
                    root.setOnClickListener {
                        val bundle = Bundle().apply { putInt(POSITION_KEY, position + 1) }
                        findNavController().navigate(R.id.action_from_list_to_details, bundle)
                    }
                }
            }
        }

        override fun getItemCount() = words?.size ?: 0

        override fun submitList(list: List<WordEntity>?) {
            words = list
            super.submitList(if (list == null) null else ArrayList<WordEntity>(list))
        }
    }

    private object DiffUtils : DiffUtil.ItemCallback<WordEntity>() {
        override fun areContentsTheSame(oldItem: WordEntity, newItem: WordEntity) = oldItem == newItem
        override fun areItemsTheSame(oldItem: WordEntity, newItem: WordEntity) = oldItem.id == newItem.id
    }

    private inner class Holder(internal val root: View) : RecyclerView.ViewHolder(root) {
        val title = root.findViewById<AppCompatTextView>(R.id.title)!!
        val subtitle = root.findViewById<AppCompatTextView>(R.id.subtitle)!!
    }
}