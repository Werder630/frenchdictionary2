package com.werder.frenchdictionary2.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.werder.frenchdictionary2.R

class ProgressView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val center = PointF()
    private val circleRect = RectF()
    private val segment = Path()
    private val strokePaint = Paint()
    private val fillPaint = Paint()

    private val strokeW = resources.getDimension(R.dimen.progress_view_stroke_width)
    private val progressColor = ContextCompat.getColor(context, R.color.colorAccent)

    var value = 0
        set(value) {
            field = value
            invalidate()
        }

    private var radius: Float = 0f

    init {
        fillPaint.color = progressColor

        strokePaint.apply {
            color = Color.parseColor("#f8f8f8")
            style = Paint.Style.FILL
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val size = resources.getDimension(R.dimen.progress_view_diameter).toInt()
        val newW = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY)
        val newH = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY)

        super.onMeasure(newW, newH)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        center.x = width.toFloat() / 2
        center.y = height.toFloat() / 2
        radius = Math.min(width, height) / 2 - strokeW
        circleRect.set(center.x - radius, center.y - radius, center.x + radius, center.y + radius)

        setPaths()
    }

    private fun setPaths() {
        val y = center.y + radius - (2 * radius * value / 100 - 1)
        val x = center.x - Math.sqrt(Math.pow(radius.toDouble(), 2.0) - Math.pow((y - center.y).toDouble(), 2.0)).toFloat()

        val angle = Math.toDegrees(Math.atan((center.y - y).toDouble() / (x - center.x))).toFloat()
        val startAngle = 180 - angle
        val sweepAngle = 2 * angle - 180

        segment.apply {
            rewind()
            addArc(circleRect, startAngle, sweepAngle)
            close()
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawCircle(center.x, center.y, radius, strokePaint)
        canvas.drawPath(segment, fillPaint)
    }
}