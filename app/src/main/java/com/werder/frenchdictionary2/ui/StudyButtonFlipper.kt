package com.werder.frenchdictionary2.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ViewFlipper
import com.werder.frenchdictionary2.R
import kotlinx.android.synthetic.main.layout_study_flipper.view.study_button
import kotlinx.android.synthetic.main.layout_study_flipper.view.unstudy_button

private const val STUDY_CHILD = 0
private const val UNSTUDY_CHILD = 1

class StudyButtonFlipper @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null
) : ViewFlipper(context, attrs) {

    init {
        View.inflate(context, R.layout.layout_study_flipper, this)

        animateFirstView = true
        inAnimation = AnimationUtils.loadAnimation(context, R.anim.study_button_in_anim)
        outAnimation = AnimationUtils.loadAnimation(context, R.anim.study_button_out_anim)

        study_button.setOnClickListener { showNext() }
        unstudy_button.setOnClickListener { showNext() }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        val listener = View.OnClickListener {
            l?.onClick(it)
            showNext()
        }
        study_button.setOnClickListener(listener)
        unstudy_button.setOnClickListener(listener)
    }

    fun showStudy() {
        if (displayedChild != STUDY_CHILD) {
            showNext()
        }
    }

    fun showUnstudy() {
        if (displayedChild != UNSTUDY_CHILD) {
            showNext()
        }
    }
}
