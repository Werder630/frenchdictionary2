package com.werder.frenchdictionary2.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.werder.frenchdictionary2.R
import com.werder.frenchdictionary2.data.WordViewModel
import com.werder.frenchdictionary2.db.WordEntity
import com.werder.frenchdictionary2.ext.onEnd
import kotlinx.android.synthetic.main.fragment_details.author
import kotlinx.android.synthetic.main.fragment_details.collapsingToolbar
import kotlinx.android.synthetic.main.fragment_details.content_container
import kotlinx.android.synthetic.main.fragment_details.example
import kotlinx.android.synthetic.main.fragment_details.example_translate
import kotlinx.android.synthetic.main.fragment_details.remember_action
import kotlinx.android.synthetic.main.fragment_details.source
import kotlinx.android.synthetic.main.fragment_details.toolbar
import kotlinx.android.synthetic.main.fragment_details.translate
import kotlinx.android.synthetic.main.fragment_details.voice
import java.util.*

import kotlinx.android.synthetic.main.fragment_details.remember_action as rememberAction

class DetailsFragment : Fragment(), TextToSpeech.OnInitListener {

    private lateinit var tts: TextToSpeech

    private val viewModel: WordViewModel by lazy { ViewModelProviders.of(this).get(WordViewModel::class.java) }

    private val animLeftOut: Animation by lazy { AnimationUtils.loadAnimation(requireContext(), R.anim.detail_exit_anim) }
    private val animLeftIn: Animation by lazy { AnimationUtils.loadAnimation(requireContext(), R.anim.detail_enter_anim) }
    private val animRightOut: Animation by lazy { AnimationUtils.loadAnimation(requireContext(), R.anim.detail_pop_exit_anim) }
    private val animRightIn: Animation by lazy { AnimationUtils.loadAnimation(requireContext(), R.anim.detail_pop_enter_anim) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val root = inflater.inflate(R.layout.fragment_details, container, false)
        setHasOptionsMenu(true)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)!!.setSupportActionBar(toolbar)

        tts = TextToSpeech(activity, this)

        content_container.setOnTouchListener(SimpleOnTouchListener {
            if (it > 0)
                anim(animRightOut, animRightIn) { viewModel.previous() }
            else
                anim(animLeftOut, animLeftIn) { viewModel.next() }
        })

        viewModel.currentWord.observe(this, Observer<WordEntity> {
            refreshDetails(it)
        })

        arguments?.let {
            viewModel.currentPosition = it.getInt(POSITION_KEY)
        }

        voice.setOnClickListener {
            val text = collapsingToolbar.title.toString()
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, "Id")
        }

        rememberAction.setOnClickListener {
            viewModel.updateRememberState()
        }
    }

    override fun onDestroy() {
        tts.apply {
            stop()
            shutdown()
        }
        super.onDestroy()
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            val locale = Locale.FRENCH
            val result = tts.setLanguage(locale)
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This language not supported")
                voice.hide()
            }
        } else {
            voice.hide()
        }
    }

    private fun anim(startAnim: Animation, endAnim: Animation, block: () -> Unit) {
        startAnim.onEnd {
            block()
            content_container.startAnimation(endAnim)
        }
        content_container.startAnimation(startAnim)
    }

    private fun refreshDetails(word: WordEntity) {
        collapsingToolbar.title = word.word
        translate.text = word.translate
        example.text = word.example.replace("|", "\n")
        example_translate.text = word.translateExample.replace("|", "\n")
        author.text = word.author
        source.text = "«${word.source}»"
        remember_action.apply {
            if (word.remembered) showUnstudy() else showStudy()
        }
    }

    private class SimpleOnTouchListener(
            private val block: (Int) -> Unit
    ) : View.OnTouchListener {
        var startX: Int = -1
        var startY: Int = -1

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View, event: MotionEvent): Boolean {
            if (event.action == MotionEvent.ACTION_DOWN) {
                startX = event.x.toInt()
                startY = event.y.toInt()
            } else if (event.action == MotionEvent.ACTION_MOVE) {
                if (Math.abs(event.x - startX) > 100) {
                    block(event.x.compareTo(startX))
                }
            }
            return true
        }
    }
}