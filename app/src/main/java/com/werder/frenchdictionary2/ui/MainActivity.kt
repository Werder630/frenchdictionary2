package com.werder.frenchdictionary2.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.werder.frenchdictionary2.R
import kotlinx.android.synthetic.main.activity_main.bottom_nav_view as bottomNavView

class MainActivity : AppCompatActivity() {

    private lateinit var navHostFragment: NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val controller = (supportFragmentManager.findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment).navController

        val itemSelectedListener = object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                if (bottomNavView.selectedItemId == item.itemId) return false
                return NavigationUI.onNavDestinationSelected(item, controller)
            }
        }

        controller.addOnDestinationChangedListener { contr, dest, arg ->
            bottomNavView.setOnNavigationItemSelectedListener(null)
            bottomNavView.selectedItemId = dest.id
            bottomNavView.setOnNavigationItemSelectedListener(itemSelectedListener)
        }
    }
}
