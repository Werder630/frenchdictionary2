package com.werder.frenchdictionary2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.werder.frenchdictionary2.R
import com.werder.frenchdictionary2.data.WordViewModel
import kotlinx.android.synthetic.main.fragment_statistic.count
import kotlinx.android.synthetic.main.fragment_statistic.progress
import kotlinx.android.synthetic.main.fragment_statistic.toolbar
import kotlinx.android.synthetic.main.fragment_statistic.percent as percentTV

class StatisticFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_statistic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setTitle(R.string.title_stat)

        val viewModel = ViewModelProviders.of(this).get(WordViewModel::class.java)
        val allCount = viewModel.allWordCount()
        val rememberedWordCount = viewModel.rememberedWordCount()
        val percent = (rememberedWordCount * 100 / allCount)

        progress.value = percent
        count.text = "$rememberedWordCount / $allCount"
        percentTV.text = "$percent%"
    }
}